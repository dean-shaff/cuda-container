#include <vector>
#include <complex>
#include <iostream>

#include "catch.hpp"

#include "CUDAContainer.hpp"


TEMPLATE_TEST_CASE (
  "CUDAContainer handles memory on GPU",
  "[unit][cuda][CUDAContainer]",
  cufftComplex, cufftDoubleComplex
)
{

  unsigned size = 100;
  typedef typename cuda_type_map<TestType>::type complex_type;
  typedef typename cuda_type_map<TestType>::float_type type;


  SECTION ("Can allocate data on device")
  {
    CUDAContainer<TestType> container(size);
    REQUIRE(container.size() == size);
  }

  SECTION ("Can allocate and copy data in constructor with vector")
  {
    std::vector<complex_type> vec(size);
    CUDAContainer<TestType> container (vec);
    REQUIRE(vec.size() == container.size());
  }

  SECTION ("Can allocate and copy data in constructor with pointer")
  {
    std::vector<complex_type> vec(size);
    CUDAContainer<TestType> container (vec.data(), vec.size());
    REQUIRE(vec.size() == container.size());
  }

  SECTION ("Can copy data from vector to device")
  {
    CUDAContainer<TestType> container(size);
    std::vector<complex_type> vec(size + 1);
    container.h2d(vec);
    REQUIRE(container.size() == vec.size());
  }

  SECTION ("Can copy data from pointer to device")
  {
    CUDAContainer<TestType> container(size);
    std::vector<complex_type> vec(size + 1);
    container.h2d(vec.data(), vec.size());
    REQUIRE(container.size() == vec.size());
  }

  SECTION ("Can copy data from device to vector")
  {
    CUDAContainer<TestType> container(size);
    std::vector<complex_type> vec;
    container.d2h(vec);
    REQUIRE(vec.size() == container.size());
  }

  SECTION ("Can copy data from device to pointer")
  {
    CUDAContainer<TestType> container(size);
    std::vector<complex_type> vec(size);
    container.d2h(vec.data());
    REQUIRE(vec.size() == container.size());
  }

  SECTION ("Copies correct data to device and back")
  {
    std::vector<complex_type> expected(size);
    std::vector<complex_type> test_vec;
    // auto rand_gen = util::random<type>();
    for (unsigned idx=0; idx<size; idx++)
    {
      expected[idx] = complex_type((type) idx, (type) idx);
    }
    CUDAContainer<TestType> container(expected);

    container.d2h(test_vec);

    bool allclose = true;
    unsigned nclose = 0;
    for (unsigned idx=0; idx<size; idx++)
    {
      if (expected[idx] != test_vec[idx]) {
        allclose = false;
      } else {
        nclose++;
      }
    }

    std::cerr << "test_pfb_util: "
      << nclose << "/" << size
      << " (" << 100 * nclose / size
      << "%) equal" << std::endl;
    REQUIRE(allclose == true);

  }
}
