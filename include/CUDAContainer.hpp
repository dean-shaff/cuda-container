#ifndef __cuda_container_hpp
#define __cuda_container_hpp

#include <complex>
#include <vector>

#include <cuda_runtime.h>
#include <cufft.h>

template<typename dtype>
struct cuda_type_map;

template<>
struct cuda_type_map<cufftComplex> {
  typedef std::complex<float> type;
  typedef float float_type;
};

template<>
struct cuda_type_map<cufftDoubleComplex> {
  typedef std::complex<double> type;
  typedef double float_type;
};

template<typename dtype>
class CUDAContainer {
public:

  CUDAContainer (unsigned size);

  CUDAContainer (const std::vector<typename cuda_type_map<dtype>::type>& data);

  CUDAContainer (const typename cuda_type_map<dtype>::type* data, unsigned size);

  ~CUDAContainer ();

  dtype* data () { return device_data; }

  unsigned size () { return nelements; }

  void h2d (const typename cuda_type_map<dtype>::type* data, unsigned size);

  void h2d (const std::vector<typename cuda_type_map<dtype>::type>& data);

  void d2h (typename cuda_type_map<dtype>::type* data);

  void d2h (std::vector<typename cuda_type_map<dtype>::type>& data);


private:
  dtype* device_data;
  unsigned nelements;

};

template<typename dtype>
CUDAContainer<dtype>::CUDAContainer (
  unsigned size
) : nelements(size)
{
  cudaMalloc((void **) &device_data, sizeof(dtype) * nelements);
}


template<typename dtype>
CUDAContainer<dtype>::CUDAContainer (
  const std::vector<typename cuda_type_map<dtype>::type>& data
)
{
  nelements = data.size();
  cudaMalloc((void **) &device_data, sizeof(dtype) * nelements);
  h2d(data);
}
template<typename dtype>
CUDAContainer<dtype>::CUDAContainer (
  const typename cuda_type_map<dtype>::type* data, unsigned size
)
{
  nelements = size;
  cudaMalloc((void **) &device_data, sizeof(dtype) * nelements);
  h2d(data, size);
}


template<typename dtype>
CUDAContainer<dtype>::~CUDAContainer ()
{
  cudaFree(device_data);
}


template<typename dtype>
void CUDAContainer<dtype>::h2d (
  const typename cuda_type_map<dtype>::type* data, unsigned size
)
{
  if (size > nelements) {
    cudaFree(device_data);
    nelements = size;
    cudaMalloc((void **) &device_data, sizeof(dtype) * nelements);
  }

  cudaMemcpy(
    device_data,
    (dtype*) data,
    nelements*sizeof(dtype),
    cudaMemcpyHostToDevice);
}



template<typename dtype>
void CUDAContainer<dtype>::h2d (
  const std::vector<typename cuda_type_map<dtype>::type>& data
)
{
  h2d(data.data(), data.size());
}

template<typename dtype>
void CUDAContainer<dtype>::d2h (
  typename cuda_type_map<dtype>::type* data
)
{
  cudaMemcpy(
    (dtype*) data,
    device_data,
    nelements*sizeof(dtype),
    cudaMemcpyDeviceToHost);
}



template<typename dtype>
void CUDAContainer<dtype>::d2h (
  std::vector<typename cuda_type_map<dtype>::type>& data
)
{
  if (data.size() < nelements) {
    data.resize(nelements);
  }
  d2h(data.data());
}

#endif
