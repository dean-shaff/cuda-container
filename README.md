## CUDA Container

Simple wrapper around CUDA C-style memory allocation layer. Not all C++ conventions are currently implemented. Unlike `thrust::device_vector`, `CUDAContainer` can be used inside C++ source files.
